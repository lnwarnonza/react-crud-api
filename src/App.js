import Button from "@mui/material/Button";
import Navbar from "./Navbar.js";
import User from "./User.js";
import Bar from "./Bar.js";
function App() {
  return (
    <div>
      <Navbar />

      <User />
    </div>
  );
}

export default App;
